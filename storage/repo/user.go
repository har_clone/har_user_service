package repo

import "time"

type User struct {
	Id              int64
	FirstName       string
	LastName        string
	Username        string
	Password        string
	ProfileImageUrl string
	Bio             string
	Email           string
	Gender          string
	PhoneNumber     string
	Type            string
	CreatedAt       time.Time
}

type UserResponse struct {
	Id              int64
	FirstName       string
	LastName        string
	Username        string
	ProfileImageUrl string
	Bio             string
	Email           string
	Gender          string
	PhoneNumber     string
	Type            string
	CreatedAt       time.Time
}

type CreateUserReq struct {
	FirstName       string
	LastName        string
	Username        string
	Password        string
	Email           string
	PhoneNumber     string
	Gender          string
	Type            string
	Bio             string
}

type ChangeUser struct {
	Id          int64
	FirstName   string
	LastName    string
	Username    string
	Bio         string
	PhoneNumber string
	Gender      string
}

type GetAllUsersRequest struct {
	Limit      int32
	Page       int32
	Search     string
	SortByDate string
}

type GetAllUsersResponse struct {
	Users []*UserResponse
	Count int64
}

type ChangePassword struct {
	UserId      int64
	NewPassword string
}

type CheckData struct {
	Email       string
	Username    string
	PhoneNumber string
}

type UserImageUploadRequest struct {
	UserId   int64
	ImageUrl string
}

type UserStorageI interface {
	CreateUser(*CreateUserReq) (*User, error)
	GetUser(int64) (*UserResponse, error)
	GetAllUser(*GetAllUsersRequest) (*GetAllUsersResponse, error)
	UserImageUpload(*UserImageUploadRequest) (*UserResponse, error)
	GetUserByEmail(email *CheckData) (*User, error)
	UpdateUser(*ChangeUser) (*UserResponse, error)
	UpdatePassword(*ChangePassword) error
	DeleteUser(int64) error
}
