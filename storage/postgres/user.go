package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/har_clone/har_user_service/pkg/utils"
	"gitlab.com/har_clone/har_user_service/storage/repo"
)

type userRepo struct {
	db *sqlx.DB
}

func NewUser(db *sqlx.DB) repo.UserStorageI {
	return &userRepo{
		db: db,
	}
}

func (ur *userRepo) CreateUser(user *repo.CreateUserReq) (*repo.User, error) {
	var ResponseUser repo.User
	fmt.Println("Storage")
	fmt.Println(user)
	fmt.Println("Storage")
	query := `
		INSERT INTO users(
			first_name,
			last_name,
			username,
			bio,
			password,
			email,
			phone_number,
			gender,
			type
		)values($1,$2,$3,$4,$5,$6,$7,$8,$9)
		RETURNING 
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			password,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			created_at
	`

	row := ur.db.QueryRow(
		query,
		user.FirstName,
		utils.NullString(user.LastName),
		utils.NullString(user.Username),
		utils.NullString(user.Bio),
		user.Password,
		user.Email,
		utils.NullString(user.PhoneNumber),
		user.Gender,
		user.Type,
	)

	if err := row.Scan(
		&ResponseUser.Id,
		&ResponseUser.FirstName,
		&ResponseUser.LastName,
		&ResponseUser.Username,
		&ResponseUser.Password,
		&ResponseUser.ProfileImageUrl,
		&ResponseUser.Bio,
		&ResponseUser.Email,
		&ResponseUser.Gender,
		&ResponseUser.PhoneNumber,
		&ResponseUser.Type,
		&ResponseUser.CreatedAt,
	); err != nil {
		return &repo.User{}, err
	}
	fmt.Println(user)
	fmt.Println(ResponseUser)
	return &ResponseUser, nil
}

func (ur *userRepo) GetUser(ID int64) (*repo.UserResponse, error) {
	var ResponseUser repo.UserResponse
	query := `
		SELECT
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			created_at
		FROM users
		where id = $1
	`

	row := ur.db.QueryRow(query, ID)

	if err := row.Scan(
		&ResponseUser.Id,
		&ResponseUser.FirstName,
		&ResponseUser.LastName,
		&ResponseUser.Username,
		&ResponseUser.ProfileImageUrl,
		&ResponseUser.Bio,
		&ResponseUser.Email,
		&ResponseUser.Gender,
		&ResponseUser.PhoneNumber,
		&ResponseUser.Type,
		&ResponseUser.CreatedAt,
	); err != nil {
		return &repo.UserResponse{}, err
	}

	return &ResponseUser, nil
}

func (ur *userRepo) GetAllUser(param *repo.GetAllUsersRequest) (*repo.GetAllUsersResponse, error) {
	Result := repo.GetAllUsersResponse{
		Users: make([]*repo.UserResponse, 0),
	}

	offset := (param.Page - 1) * param.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", param.Limit, offset)

	filter := fmt.Sprintf("where type like '%s' ", "user")

	if param.Search != "" {
		str := "%" + param.Search + "%"
		filter += fmt.Sprintf(`
			and (first_name ILIKE '%s' OR last_name ILIKE '%s' OR email ILIKE '%s' 
				OR username ILIKE '%s' OR phone_number ILIKE '%s') `,
			str, str, str, str, str,
		)
	}

	if param.SortByDate == "" {
		param.SortByDate = "desc"
	}

	query := `
		SELECT
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			created_at
		FROM users
		` + filter + `
		ORDER BY created_at ` + param.SortByDate + ` ` + limit

	rows, err := ur.db.Query(query)
	if err != nil {
		return &repo.GetAllUsersResponse{}, err
	}

	defer rows.Close()

	for rows.Next() {
		var user repo.UserResponse
		err := rows.Scan(
			&user.Id,
			&user.FirstName,
			&user.LastName,
			&user.Username,
			&user.ProfileImageUrl,
			&user.Bio,
			&user.Email,
			&user.Gender,
			&user.PhoneNumber,
			&user.Type,
			&user.CreatedAt,
		)
		if err != nil {
			return &repo.GetAllUsersResponse{}, err
		}

		Result.Users = append(Result.Users, &user)
	}

	queryCount := `SELECT count(1) FROM users ` + filter
	err = ur.db.QueryRow(queryCount).Scan(&Result.Count)
	if err != nil {
		return &repo.GetAllUsersResponse{}, err
	}

	return &Result, nil
}

func (ur *userRepo) UpdateUser(user *repo.ChangeUser) (*repo.UserResponse, error) {
	var ResponseUser repo.UserResponse
	query := `
		UPDATE users SET 
			first_name=$1,
			last_name=$2,
			username=$3,
			phone_number=$4,
			gender=$5,
			bio=$6
		where id=$7
		RETURNING 
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			created_at
	`
	row := ur.db.QueryRow(
		query,
		user.FirstName,
		utils.NullString(user.LastName),
		utils.NullString(user.Username),
		utils.NullString(user.PhoneNumber),
		user.Gender,
		user.Bio,
		user.Id,
	)

	if err := row.Scan(
		&ResponseUser.Id,
		&ResponseUser.FirstName,
		&ResponseUser.LastName,
		&ResponseUser.Username,
		&ResponseUser.ProfileImageUrl,
		&ResponseUser.Bio,
		&ResponseUser.Email,
		&ResponseUser.Gender,
		&ResponseUser.PhoneNumber,
		&ResponseUser.Type,
		&ResponseUser.CreatedAt,
	); err != nil {
		return &repo.UserResponse{}, err
	}

	return &ResponseUser, nil
}

func (ur *userRepo) DeleteUser(ID int64) error {
	effect, err := ur.db.Exec("delete from users where id=$1", ID)
	if err != nil {
		return err
	}
	rowsCount, err := effect.RowsAffected()
	if err != nil {
		return err
	}
	if rowsCount == 0 {
		return sql.ErrNoRows
	}
	return nil
}

func (ur *userRepo) GetUserByEmail(data *repo.CheckData) (*repo.User, error) {
	var ResponseUser repo.User
	query := `
		SELECT 
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			password,
			created_at
		FROM users
		where email=$1 or username=$2 or phone_number=$3
	`

	row := ur.db.QueryRow(query, data.Email, data.Username, data.PhoneNumber)

	if err := row.Scan(
		&ResponseUser.Id,
		&ResponseUser.FirstName,
		&ResponseUser.LastName,
		&ResponseUser.Username,
		&ResponseUser.ProfileImageUrl,
		&ResponseUser.Bio,
		&ResponseUser.Email,
		&ResponseUser.Gender,
		&ResponseUser.PhoneNumber,
		&ResponseUser.Type,
		&ResponseUser.Password,
		&ResponseUser.CreatedAt,
	); err != nil {
		return &repo.User{}, err
	}

	return &ResponseUser, nil
}

func (ur *userRepo) UpdatePassword(newPassword *repo.ChangePassword) error {
	query := `
		update users set
			password=$1
		where id=$2
	`
	effect, err := ur.db.Exec(query, newPassword.NewPassword, newPassword.UserId)
	if err != nil {
		return err
	}
	rowsCount, err := effect.RowsAffected()
	if err != nil {
		return err
	}
	if rowsCount == 0 {
		return sql.ErrNoRows
	}
	return nil
}

func (ur *userRepo) UserImageUpload(req *repo.UserImageUploadRequest) (*repo.UserResponse, error) {
	var result repo.UserResponse
	row := ur.db.QueryRow(`
		UPDATE users SET 
			profile_image_url=$1 
		WHERE id=$2
		RETURNING
			id,
			first_name,
			COALESCE(last_name,'') as last_name,
			COALESCE(username,'') as username,
			COALESCE(profile_image_url,'') as profile_image_url,
			COALESCE(bio,'') as bio,
			email,
			gender,
			COALESCE(phone_number,'') as phone_number,
			type,
			created_at
	`, req.ImageUrl, req.UserId)
	if err := row.Scan(
		&result.Id,
		&result.FirstName,
		&result.LastName,
		&result.Username,
		&result.ProfileImageUrl,
		&result.Bio,
		&result.Email,
		&result.Gender,
		&result.PhoneNumber,
		&result.Type,
		&result.CreatedAt,
	); err != nil {
		return &repo.UserResponse{}, err
	}
	return &result, nil
}
