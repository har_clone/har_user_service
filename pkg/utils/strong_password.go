package utils


func IsStrongPassword(password string)bool{
	length:=len(password)>7
	number:=false
	symbol:=false
	IsLower:=false
	IsUpper:=false
	for _,i:=range password{
		if 64<i && i<91{
			IsUpper=true
		}else if 96<i && i<123{
			IsLower=true
		}else if 47<i && i<58{
			number=true
		}else {
			symbol=true
		}
	}
	return length && number && symbol && IsLower && IsUpper
}