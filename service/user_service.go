package service

import (
	"context"
	"errors"
	"time"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	pb "gitlab.com/har_clone/har_user_service/genproto/user_service"
	"gitlab.com/har_clone/har_user_service/pkg/utils"
	"gitlab.com/har_clone/har_user_service/storage"
	"gitlab.com/har_clone/har_user_service/storage/repo"
)

func parseUserModel(user *repo.UserResponse) *pb.User {
	return &pb.User{
		Id:              user.Id,
		FirstName:       user.FirstName,
		LastName:        user.LastName,
		Username:        user.Username,
		ProfileImageUrl: user.ProfileImageUrl,
		Bio:             user.Bio,
		Email:           user.Email,
		Gender:          user.Gender,
		PhoneNumber:     user.PhoneNumber,
		Type:            user.Type,
		CreatedAt:       user.CreatedAt.Format(time.RFC3339),
	}
}

const (
	IsNotStrongPassword = "this password is not strong"
)

type UserService struct {
	pb.UnimplementedUserServiceServer
	storage storage.StorageI
}

func NewUserService(strg storage.StorageI) *UserService {
	return &UserService{
		storage:                        strg,
		UnimplementedUserServiceServer: pb.UnimplementedUserServiceServer{},
	}
}

func (s *UserService) CreateUser(ctx context.Context, req *pb.CreateUserRequest) (*pb.User, error) {
	if !utils.IsStrongPassword(req.Password) {
		return &pb.User{}, errors.New(IsNotStrongPassword)
	}
	hashedPassword, err := utils.HashPassword(req.Password)
	if err != nil {
		return &pb.User{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}
	user, err := s.storage.User().CreateUser(&repo.CreateUserReq{
		FirstName:   req.FirstName,
		LastName:    req.LastName,
		Username:    req.Username,
		Password:    hashedPassword,
		Email:       req.Email,
		PhoneNumber: req.PhoneNumber,
		Gender:      req.Gender,
		Bio:         req.Bio,
		Type:        req.Type,
	})
	if err != nil {
		return &pb.User{}, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}
	return &pb.User{
		Id:              user.Id,
		FirstName:       user.FirstName,
		LastName:        user.LastName,
		Username:        user.Username,
		ProfileImageUrl: user.ProfileImageUrl,
		Bio:             user.Bio,
		Email:           user.Email,
		Gender:          user.Gender,
		PhoneNumber:     user.PhoneNumber,
		Type:            user.Type,
		CreatedAt:       user.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *UserService) GetUser(ctx context.Context, req *pb.UserIdRequest) (*pb.User, error) {
	resp, err := s.storage.User().GetUser(req.Id)
	if err != nil {
		return &pb.User{}, err
	}
	return parseUserModel(resp), nil
}

func (s *UserService) GetAllUsers(ctx context.Context, req *pb.GetAllUsersRequest) (*pb.GetAllUsersResponse, error) {
	result, err := s.storage.User().GetAllUser(&repo.GetAllUsersRequest{
		Page:       req.Page,
		Limit:      req.Limit,
		Search:     req.Search,
		SortByDate: req.SortByDate,
	})
	if err != nil {
		return &pb.GetAllUsersResponse{}, err
	}
	response := pb.GetAllUsersResponse{}
	response.Count = result.Count
	for _, i := range result.Users {
		response.Users = append(response.Users, parseUserModel(i))
	}
	return &response, nil
}

func (s *UserService) UserImageUpload(ctx context.Context, req *pb.UserImageUploadRequest) (*pb.User, error) {
	user, err := s.storage.User().UserImageUpload(&repo.UserImageUploadRequest{
		UserId:   req.UserId,
		ImageUrl: req.ImageUrl,
	})
	if err != nil {
		return &pb.User{}, err
	}

	return parseUserModel(user), nil
}

func (s *UserService) GetUserByEmail(ctx context.Context, req *pb.CheckData) (*pb.User, error) {
	user, err := s.storage.User().GetUserByEmail(&repo.CheckData{
		Email:       req.Email,
		Username:    req.Username,
		PhoneNumber: req.PhoneNumber,
	})

	if err != nil {
		return &pb.User{}, err
	}
	return &pb.User{
		Id:              user.Id,
		FirstName:       user.FirstName,
		LastName:        user.LastName,
		Username:        user.Username,
		Password:        user.Password,
		ProfileImageUrl: user.ProfileImageUrl,
		Bio:             user.Bio,
		Email:           user.Email,
		Gender:          user.Gender,
		PhoneNumber:     user.PhoneNumber,
		Type:            user.Type,
		CreatedAt:       user.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *UserService) UpdateUser(ctx context.Context, req *pb.ChangeUser) (*pb.User, error) {
	user, err := s.storage.User().UpdateUser(&repo.ChangeUser{
		Id:          req.Id,
		FirstName:   req.FirstName,
		LastName:    req.LastName,
		Username:    req.Username,
		Bio:         req.Bio,
		PhoneNumber: req.PhoneNumber,
		Gender:      req.Gender,
	})
	if err != nil {
		return &pb.User{}, err
	}

	return parseUserModel(user), nil
}

func (s *UserService) UpdatePassword(ctx context.Context, req *pb.ChangePassword) (*emptypb.Empty, error) {
	if err := s.storage.User().UpdatePassword(&repo.ChangePassword{
		UserId:      req.UserId,
		NewPassword: req.NewPassword,
	}); err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, nil
}

func (s *UserService) DeleteUser(ctx context.Context, req *pb.UserIdRequest) (*emptypb.Empty, error) {
	if err := s.storage.User().DeleteUser(int64(req.Id)); err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, nil
}
