CREATE TABLE "users"(
    "id" serial primary key,
    "first_name" VARCHAR(255) NOT NULL,
    "last_name" VARCHAR(255),
    "username" VARCHAR(255) UNIQUE ,
    "bio" VARCHAR(255),
    "profile_image_url" VARCHAR(255),
    "password" VARCHAR(255) NOT NULL,
    "email" VARCHAR(255) NOT NULL UNIQUE,
    "phone_number" VARCHAR(255) UNIQUE,
    "gender" VARCHAR(10) CHECK ("gender" IN('male', 'female')),
    "type" VARCHAR(255),
    "created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
